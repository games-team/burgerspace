/*  $Id: BurgerSpaceEngine.h,v 1.42 2024/05/25 03:03:08 sarrazip Exp $
    BurgerSpaceEngine.h - Main engine

    burgerspace - A hamburger-smashing video game.
    Copyright (C) 2001-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef _H_BurgerSpaceEngine
#define _H_BurgerSpaceEngine

#include "IngredientGroup.h"
#include "IngredientSprite.h"
#include "Controller.h"

#include <flatzebra/GameEngine.h>
#include <flatzebra/Sprite.h>
#include <flatzebra/SoundMixer.h>
#include <flatzebra/KeyState.h>

#include <string>
#include <iostream>
#include <map>


class BurgerSpaceEngine : public flatzebra::GameEngine
{
public:

    // _hideLandedEnemies: false for behavior of versions <= 1.9.4.
    //
    BurgerSpaceEngine(bool _fullScreen,
                      bool _processActiveEvent,
                      bool _useAcceleratedRendering,
                      bool _useSound,
                      SDL_Keycode _pepperKey,
                      int _initLevelNumber,
                      bool _oldMotionMode,
                      bool _hideLandedEnemies);

    virtual ~BurgerSpaceEngine();

    // Returns true if the game must continue, or false to have it stop.
    bool update();

    enum TileType : uint8_t
    {
        INVALID_TILE,
        EMPTY_TILE,
        FLOOR_TILE,
        FLOOR_AND_LADDER_TILE,
        LADDER_TILE,
        PLATE_TILE,
    };

    // Inherited from flatzebra::GameEngine.
    virtual void processKey(SDL_Keycode keycode, bool pressed) override;

    // Inherited from flatzebra::GameEngine.
    virtual void processActivation(bool appActive) override;

    // Inherited from flatzebra::GameEngine.
    virtual bool tick() override;

    void updateLevel(int levelNo, size_t numRows, size_t numColumns,
                             flatzebra::Couple levelPos, const std::string &desc);

    void updateScore(long theScore, int numLives, int numAvailablePeppers, int cumulLevelNo);

    void playSoundEffect(flatzebra::SoundMixer::Chunk &chunk);

    EnemyType chooseEnemyType();

    bool isUserControlledEnemy(const flatzebra::Sprite &s) const;

    void startNewGame();
    bool isPaused() const;
    void pauseGame();
    void resumeGame();
    void setChefRequest(const bool desiredDirections[4], bool shootPepper);
    void setEnemyRequest(const bool desiredDirections[4]);

    // Returns true for success, or false for failure.
    bool saveGame(std::ostream &out);

    // Returns 0 for success, or a negative error code for failure.
    int loadGame(std::istream &in);

private:

    ///////////////////////////////////////////////////////////////////////////
    //
    //  LOCAL TYPES, CLASSES AND CONSTANTS
    //
    //

    struct IntPair
    {
        int first, second;
    };


    struct IntQuad
    {
        int first, second, third, fourth;
    };


public:

    class Level
    {
    public:
        Level();
        ~Level();

        void init(int no, int nCols, int nRows, flatzebra::Couple posInPixels);
        void setLevelNo(int no);
        int  getLevelNo() const;
        void setTileMatrixEntry(int colNo, int rowNo, TileType type, SDL_Texture *pixmap);
        std::vector<SDL_Texture *> &getTileMatrixRow(int rowNo);
        TileType getTileTypeAtPixel(flatzebra::Couple pos) const;

        void setTextDescription(const std::string &desc);
        std::string getTextDescription() const;

    public:
        flatzebra::Couple sizeInTiles;
        flatzebra::Couple sizeInPixels;
        flatzebra::Couple positionInPixels;

    private:
        int levelNo;
        std::vector< std::vector<TileType> > tileTypeMatrix;
        std::vector< std::vector<SDL_Texture *> > tileMatrix;  // the SDL_Texture objects are not owned by this matrix
        std::string desc;  // see BurgerSpaceEngine::loadLevel()

        // Forbidden operations:
        Level(const Level &);
        Level &operator = (const Level &);
    };

protected:

    class IngInit
    {
    public:
        enum IngType
        {
            BOTTOM_BUN, MEAT, LETTUCE, RED_STUFF, YELLOW_STUFF, TOP_BUN

            /*  The red stuff is a slice of tomato, and the yellow stuff
                is cheese.  This was not known at the time when this
                enumeration was defined...
            */
        };

        int xInitTile, yInitTile, yTargetTile, rank;
        IngType type;
    };


    ///////////////////////////////////////////////////////////////////////////
    //
    //  DATA MEMBERS
    //
    //

    static const char
        *levelDescriptor1[],
        *levelDescriptor2[],
        *levelDescriptor3[],
        *levelDescriptor4[],
        *levelDescriptor5[],
        *levelDescriptor6[];

    static const char **levelDescriptorTable[];

    static const IngInit
        tableIngredientsLevel1[],
        tableIngredientsLevel2[],
        tableIngredientsLevel3[],
        tableIngredientsLevel4[],
        tableIngredientsLevel5[],
        tableIngredientsLevel6[];

    static const IngInit *tableOfTablesOfIngredientsLevel[];

    static const IntQuad enemyStartingHeights[];
    static const IntPair playerStartingPos[];


    int initLevelNo;  // level number at which play starts
    int cumulLevelNo;

    enum RequestType { NO_REQUEST, START_GAME_REQUEST, PAUSE_REQUEST, RESUME_REQUEST };

    RequestType currentRequest;
    bool paused;
    unsigned long tickCount;


    /*  PLAYER:
    */
    flatzebra::Couple initPlayerPos;  // initial player position in a level
    flatzebra::PixmapArray playerPA;
    flatzebra::Sprite *playerSprite;
    int lastPlayerDirection;
    bool oldMotionMode;
    bool desiredDirs[4];
    bool chefWantsToShootPepper;

    flatzebra::PixmapArray pepperPA;
    flatzebra::SpriteList  pepperSprites;

    /*  ENEMIES:
    */
    unsigned long timeForNewEnemy;
                // tick count at which a new enemy must be created;
                // 0 means none

    flatzebra::PixmapArray eggPA;
    flatzebra::PixmapArray hotdogPA;
    flatzebra::PixmapArray picklePA;
    flatzebra::SpriteList  enemySprites;
    unsigned enemyTypeCounter;  // used to generate new enemy sprites
    int requestedEnemyDirection;  // RIGHT, UP, LEFT, DOWN, or -1 for none


    /*  INGREDIENTS:
    */
    flatzebra::PixmapArray topBunPA;
    flatzebra::PixmapArray lettucePA;
    flatzebra::PixmapArray meatPA;
    flatzebra::PixmapArray redStuffPA;
    flatzebra::PixmapArray yellowStuffPA;
    flatzebra::PixmapArray bottomBunPA;
    IngredientSprite::List ingredientSprites;  // owns the contained objects
    IngredientGroup::List  ingredientGroups;
                                    // contained objects must come from 'new'
    bool hideLandedEnemies;  // after enemies carried by a slice have landed,
                             // they are hidden (true) or shown as frozen (false)
                             // until they revive


    /*  TREATS (icecream, etc, that the player eats to get a pepper):
    */
    flatzebra::PixmapArray treatPA;
    flatzebra::SpriteList treatSprites;
    int timeForTreat;


    /*  DIGITS:
    */
    flatzebra::PixmapArray digitPA;
    flatzebra::SpriteList scoreSprites;


    int    numHamburgersToDo;
    long   thePeakScore;  // player's best score yet in this game

    long   theScore;      // player's score in points

    bool   celebrationMode;  // used when player has just won the level

    int    numLives;  // number of player lives left

    int    numAvailablePeppers;  // number of pepper shots available to player

    Level theCurrentLevel;


    flatzebra::PixmapArray tilePixmaps;

    flatzebra::Couple scoreAreaPos;
    flatzebra::Couple scoreAreaSize;

    flatzebra::Couple numLivesAreaPos;
    flatzebra::Couple numLivesAreaSize;

    flatzebra::Couple numAvailablePeppersAreaPos;
    flatzebra::Couple numAvailablePeppersAreaSize;
    SDL_Keycode pepperKey;

    Controller controller;
    SDL_Keycode lastKeyPressed;

    flatzebra::Couple levelNoAreaPos;
    flatzebra::Couple levelNoAreaSize;

    std::string currentPauseMessage;

    enum GameMode { QUIT_DIALOG, SAVE_DIALOG, LOAD_DIALOG, IN_GAME };

    GameMode gameMode;

    size_t slotDirCursorPos;  // 1..9; used by getSlotNumber()

    /*  SOUND EFFECTS:
    */
    flatzebra::SoundMixer *theSoundMixer;  // see method playSoundEffect()
    bool useSound;
    flatzebra::SoundMixer::Chunk ingredientBouncesChunk;
    flatzebra::SoundMixer::Chunk ingredientInPlateChunk;
    flatzebra::SoundMixer::Chunk ingredientFallsChunk;
    flatzebra::SoundMixer::Chunk ingredientLoweredChunk;
    flatzebra::SoundMixer::Chunk enemyCatchesChefChunk;
    flatzebra::SoundMixer::Chunk enemyParalyzedChunk;
    flatzebra::SoundMixer::Chunk enemySmashedChunk;
    flatzebra::SoundMixer::Chunk chefThrowsPepperChunk;
    flatzebra::SoundMixer::Chunk chefGetsTreatChunk;
    flatzebra::SoundMixer::Chunk chefShootsBlanksChunk;
    flatzebra::SoundMixer::Chunk newGameStartsChunk;
    flatzebra::SoundMixer::Chunk levelFinishedChunk;
    flatzebra::SoundMixer::Chunk treatAppearsChunk;
    flatzebra::SoundMixer::Chunk treatDisappearsChunk;


    ///////////////////////////////////////////////////////////////////////////
    //
    //  IMPLEMENTATION FUNCTIONS
    //
    //
    void chooseDirectionAmongMany(bool directions[4]) const;
    int  chooseDirectionTowardTarget(
                            flatzebra::Couple startPos, flatzebra::Couple targetPos, int speedFactor,
                            const bool allowedDirections[4]) const;
    void getPlayerDesiredDirections(bool desiredDirs[4]) const;

    bool animatePlayer();
    void animateAutomaticCharacters();
    void detectCollisions();
    void drawSprites();

    void putSprite(const flatzebra::Sprite &s);
    void showInstructions();
    void initGameParameters();
    void initNextLevel(int levelNo = 0);
    void resetPlay();
    int isPositionAtSideOfStructure(flatzebra::Couple pos) const;
    void animateTemporarySprites(flatzebra::SpriteList &slist);
    void givePlayerPepper();
    void makePlayerWin();
    void makePlayerDie();
    void releaseAllCarriedEnemies();
    void detectEnemyCollisions(flatzebra::SpriteList &slist);
    void detectCollisionBetweenIngredientGroupAndEnemyList(
            const IngredientGroup &aGroup, flatzebra::SpriteList &enemies);
    bool ingredientGroupCollidesWithSprite(
                        const flatzebra::Couple groupPos, const flatzebra::Couple groupSize,
                        const flatzebra::Sprite &s) const;
    void loadLevel(int levelNo);
    void displayErrorMessage(const std::string &msg);
    void createPlayerSprite();
    void initializeSprites();
    void initializeMisc();
    void deleteSprite(flatzebra::Sprite *s);
    void deleteSprites(flatzebra::SpriteList &sl);
    void deleteSprites(IngredientSprite::List &isl);
    IngredientGroup *findIngredientGroupRightBelow(
                                        const IngredientGroup &upperGroup);
    bool isIngredientSpriteOnFloor(const IngredientSprite &s) const;
    bool spriteTouchesIngredientGroup(
                            const flatzebra::Sprite &s, const IngredientGroup &g) const;
    size_t carryEnemies(IngredientGroup &g);
    size_t carryEnemiesInList(IngredientGroup &g, flatzebra::SpriteList &slist);
    size_t releaseCarriedEnemies(IngredientGroup &g);
    void createScoreSprites(long n, flatzebra::Couple center);
    void loadPixmap(const char *filePath, flatzebra::PixmapArray &pa, size_t index);
    void loadPixmaps();
    void moveEnemyList(flatzebra::SpriteList &slist, int speedFactor);
    flatzebra::Couple getDistanceToPerfectPos(const flatzebra::Sprite &s) const;
    bool isSpriteOnFloor(const flatzebra::Sprite &s) const;
    flatzebra::Couple determineAllowedDirections(const flatzebra::Sprite &s,
                                    int speedFactor, int tolerance,
                                    bool allowedDirections[4]) const;
    flatzebra::Couple attemptMove(const flatzebra::Sprite &s, bool attemptLeft, bool attemptRight,
                        bool attemptUp, bool attemptDown,
                        int speedFactor) const;
    bool positionAllowsLeftMovement(flatzebra::Couple pos, flatzebra::Couple size) const;
    bool positionAllowsRightMovement(flatzebra::Couple pos, flatzebra::Couple size) const;
    bool spriteBottomCenterIsOnLadder(const flatzebra::Sprite &s) const;
    bool spriteBottomCenterIsOverLadder(const flatzebra::Sprite &s) const;
    void drawComplexEnemySprites(const flatzebra::SpriteList &slist, int oscBit);
    void addToScore(long n);
    void addToNumLives(int n);
    void initTimeForTreat();
    void changePauseState(bool newPauseState);
    void displayStartMessage(bool display);

    void drawQuitDialog();

    void drawSaveDialog();
    void drawLoadDialog();
    void showDialogBox(const std::string &msg);

    std::string serialize(flatzebra::Couple c) const;
    std::string serialize(const flatzebra::Sprite &s) const;
    std::string serialize(const EnemySprite &s) const;
    int encodePixmapArrayPointer(const flatzebra::PixmapArray *ptr) const;
    const flatzebra::PixmapArray *decodePixmapArrayCode(int) const;
    void serializeSpriteList(std::ostream &out,
                                    const flatzebra::SpriteList &list) const;
    void serializeEnemySpriteList(std::ostream &out,
                                    const flatzebra::SpriteList &list) const;
    std::string serialize(const IngredientSprite &is) const;
    bool deserialize(std::istream &in, flatzebra::Couple &c) const;
    flatzebra::Sprite *deserializeSprite(std::istream &in,
                                        bool enemy = false) const;
    bool deserializeSpriteList(std::istream &in,
                               flatzebra::SpriteList &list,
                               bool enemies = false);
    IngredientSprite *deserializeIngredientSprite(std::istream &in,
                                                IngredientGroup *ig) const;
    static std::string getSavedGamesDir();
    static std::string formSavedGameFilename(int slotNum);
    void showSlotDirectory();


    void loadTilePixmaps();
    void restoreBackground();
    void writeGameState();
    void displayMessage(int row, const char *msg);

    bool doQuitDialog(SDL_Keycode lastKeyPressed);

    int getSlotNumber(SDL_Keycode lastKeyPressed);
    void doSaveDialog(SDL_Keycode lastKeyPressed);
    void doLoadDialog(SDL_Keycode lastKeyPressed);

    static TileType getTileTypeFromChar(char ch);

    /*        Forbidden operations:
    */
    BurgerSpaceEngine(const BurgerSpaceEngine &);
    BurgerSpaceEngine &operator = (const BurgerSpaceEngine &);
};


#endif  /* _H_BurgerSpaceEngine */

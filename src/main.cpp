/*  $Id: main.cpp,v 1.2 2024/05/05 17:25:21 sarrazip Exp $
    client.cpp - main() function for BurgerSpace

    burgerspace - A hamburger-smashing video game.
    Copyright (C) 2001-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "BurgerSpaceEngine.h"

#include "util.h"

#include <errno.h>
#include <time.h>
#include <string.h>

#include <sys/types.h>

#ifdef HAVE_GETOPT_LONG
#include <getopt.h>
#endif

#ifndef DEFAULT_UDP_SERVER_PORT
#error DEFAULT_UDP_SERVER_PORT must be defined to valid UDP port number
#endif

using namespace std;


#ifdef HAVE_GETOPT_LONG
static struct option knownOptions[] =
{
    { "help",          no_argument,       NULL, 'h' },
    { "version",       no_argument,       NULL, 'v' },
    { "initial-level", required_argument, NULL, 'i' },
    { "ms-per-frame",  required_argument, NULL, 'm' },
    { "no-sound",      no_argument,       NULL, 'n' },
    { "full-screen",   no_argument,       NULL, 'f' },
    { "old-motion",    no_argument,       NULL, 'o' },
    { "z-for-pepper",  no_argument,       NULL, 'z' },
    { "hide-landed-enemies", no_argument, NULL, 'l' },
    { "no-active-event", no_argument,     NULL, 'a' },
    { "rendering",     required_argument, NULL, 'r' },

    { NULL, 0, NULL, 0 }  // marks the end
};


static
void
displayVersionNo()
{
    cout << PROGRAM << " " << VERSION << endl;
}


static
void
displayHelp()
{
    cout << "\n";

    displayVersionNo();

    cout <<
"\n"
"Copyright (C) 2001-2024 Pierre Sarrazin <http://sarrazip.com/>\n"
"This program is free software; you may redistribute it under the terms of\n"
"the GNU General Public License.  This program has absolutely no warranty.\n"
    ;

    cout <<
"\n"
"Known options:\n"
"--help                 Display this help page and exit\n"
"--version              Display this program's version number and exit\n"
"--initial-level=N      Start game at level N (N >= 1) [default=1]\n"
"--ms-per-frame=N       N milliseconds per animation frame [default=55]\n"
"                       Min. 1, max. 1000.  50 ms means 20 frames per second\n"
"--no-sound             Disable sound effects [default is to enable them]\n"
"--full-screen          Attempt to use full screen mode [default is window mode]\n"
"--old-motion           Use the old player motion system [default is new system]\n"
"--z-for-pepper         Use Z key to shoot pepper [default is Ctrl key]\n"
"--hide-landed-enemies  After an enemy lands in a plate, hide it until it revives\n"
"--no-active-event      Do not pause when the window becomes inactive.\n"
"--rendering=X          Specify the type of rendering.\n"
"                       Replace X with 'a' for accelerated, 's' for software.\n"
"\n"
    ;
}
#endif  /* HAVE_GETOPT_LONG */


int
main(int argc, char *argv[])
{
    /*  Default game parameters:
    */
    int initLevelNo = 1;
    int millisecondsPerFrame = 55;
    bool useSound = true;
    bool fullScreen = false;
    bool useOldMotionMode = false;
    bool hideLandedEnemies = false;
    SDL_Keycode pepperKey = SDLK_LCTRL;
    bool processActiveEvent = true;  // if true, SDL_ACTIVEEVENT gets processed (see BurgerSpaceClient::runClientMode())
    bool useAcceleratedRendering = true;


    #ifdef HAVE_GETOPT_LONG

    /*  Interpret the command-line options:
    */
    int c;
    do
    {
        c = getopt_long(argc, argv, "", knownOptions, NULL);

        switch (c)
        {
            case EOF:
                break;  // nothing to do

            case 'i':
                {
                    errno = 0;
                    long n = strtol(optarg, NULL, 10);
                    if (errno == ERANGE || n < 1 || n > 500)
                    {
                        cout << "Invalid initial level number.\n";
                        displayHelp();
                        return EXIT_FAILURE;
                    }

                    initLevelNo = int(n);
                }
                break;

            case 'm':
                {
                    errno = 0;
                    long n = strtol(optarg, NULL, 10);
                    if (errno == ERANGE || n < 1 || n > 1000)
                    {
                        cout << "Invalid number of ms per frame.\n";
                        displayHelp();
                        return EXIT_FAILURE;
                    }

                    millisecondsPerFrame = int(n);
                }
                break;

            case 'n':
                useSound = false;
                break;

            case 'f':
                fullScreen = true;
                break;

            case 'o':
                useOldMotionMode = true;
                break;

            case 'l':
                hideLandedEnemies = true;  // behavior of versions <= 1.9.4
                break;

            case 'z':
                pepperKey = SDLK_z;
                break;

            case 'a':  // do not process SDL_ACTIVEEVENT, i.e., do not pause when window loses focus
                processActiveEvent = false;
                break;
            
            case 'r':
                if (!strcmp(optarg, "a"))
                    useAcceleratedRendering = true;
                else if (!strcmp(optarg, "s"))
                    useAcceleratedRendering = false;
                else
                {
                    cout << "Invalid argument for --rendering.\n";
                    displayHelp();
                    return EXIT_FAILURE;
                }
                break;

            case 'v':
                displayVersionNo();
                return EXIT_SUCCESS;

            case 'h':
                displayHelp();
                return EXIT_SUCCESS;

            default:
                displayHelp();
                return EXIT_FAILURE;
        }
    } while (c != EOF && c != '?');

    #elif defined(_MSC_VER)

    if (argc >= 2)
    {
        if (strcmp(argv[1], "--help") == 0)
        {
            cout << PROGRAM << " " << VERSION << ": copyrighted and distributed under GNU GPL\n";
            return EXIT_SUCCESS;
        }
    }

    #endif  /* defined(_MSC_VER) */


    /*  Initialize the random number generator:
    */
    const char *s = getenv("SEED");
    unsigned int seed = (unsigned int) (s != NULL ? atol(s) : time(NULL));
    #ifndef NDEBUG
    cerr << "seed = " << seed << endl;
    cerr << "init-level-no = " << initLevelNo << endl;
    #endif
    srand(seed);


    try
    {
        BurgerSpaceEngine theBurgerSpaceEngine(
                                        fullScreen,
                                        processActiveEvent,
                                        useAcceleratedRendering,
                                        useSound,
                                        pepperKey,
                                        initLevelNo,
                                        useOldMotionMode,
                                        hideLandedEnemies);

        theBurgerSpaceEngine.run(millisecondsPerFrame);
    }
    catch (const string &s)
    {
        cerr << PROGRAM << ": init error: " << s << endl;
        return EXIT_FAILURE;
    }
    catch (int e)
    {
        cerr << PROGRAM << ": init error # " << e << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


#ifdef _MSC_VER
int CALLBACK WinMain(__in HINSTANCE hInstance,
                     __in HINSTANCE hPrevInstance,
                     __in LPSTR lpCmdLine,
                     __in int nCmdShow)
{
    char *argv[] = { PROGRAM, NULL };
    return main(1, argv);
}
#endif  // def _MSC_VER
